FROM centos:7

RUN yum update -y && yum -y install python 3.7 python3-pip && yum -y clean all  && rm -rf /var/cache

RUN pip3 install flask flask-jsonpify flask-restful
RUN mkdir -p /python_api
COPY python-api.py /python_api

CMD ["python3", "/python_api/python-api.py"]
